<?php

$data = array();

if( isset( $_GET['uploadfiles'] ) ){
    $error = false;
    $files = array();

    $uploaddir = '../uploads/'; // . - текущая папка где находится submit.php

    // Создадим папку если её нет

    if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );

    // переместим файлы из временной директории в указанную
    foreach( $_FILES as $file ){
        $filename = basename($file['name']);
        $ext = substr(strrchr($filename, '.'), 1);
        $nameext = time() . mt_rand() . '.' . $ext;
        if( move_uploaded_file( $file['tmp_name'], $uploaddir . $nameext ) ){
            $files[] = realpath( $uploaddir . $uploaddir . $nameext );
        }
        else{
            $error = true;
        }
    }
    $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
    echo json_encode( $data );
}