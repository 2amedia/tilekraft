$(document).ready(function () {

    $(".look-more").hover(
        function () {
        $(this).parent().find('.facing_tiles').css('opacity', '0');
    },
        function () {
            $(this).parent().find('.facing_tiles').css('opacity', '1');
        }

);

    $(".text-click").click(function () {

        $(this).siblings(".text-slide").slideToggle("slow");
        $(this).find(".up").toggleClass('show');
        $(this).find('.down').toggleClass('hidden');
    });

    repairBsModal();

    $('.bxslider').bxSlider({
        controls: true,
        pager: true,
        auto: true,
        mode: 'fade',
        speed: 700,
        responsive: true,
        preventDefaultSwipeY: true
    });

    $('#furniture, #mosaic, #mirror, #plumbing, #grout, #glue') // sliders in modals
        .on(
            'shown.bs.modal',
            function () {
                let el = $(this);
                let slider;
                setTimeout(function () {
                    slider = $(el).find('.prod-slider')
                        .show()
                        .bxSlider({
                            controls: false,
                            pager: false,
                            auto: true,
                            mode: 'fade',
                            speed: 1000,
                            responsive: true,
                            infiniteLoop: true,
                            adaptiveHeight: true,
                            addinguseCSS: false
                });

                    el.on(
                        'hide.bs.modal',
                        function () {
                            slider.destroySlider();
                            slider.css('display', 'none');
                        }
                    );
                }, 200);
            }
        );

    $('[data-modal="collection"]').on('click', function () {
        let letters = $(this).data('text');
        $('#modal_tf_sugar').find('.text-text').text(letters);
    });

    $('#modal_tf_sugar')
        .on(
            'shown.bs.modal',
            () => setTimeout(() => slider_init(), 200)
        );

    $(".gallery").fancybox();

    $('[type="phone"]').each(function () {
        $(this)
            .on('input', mask)
            .on('blur', mask)
            .on('focus', mask);
    });

    //scrolling to blocks with margin menu
    $('.nav-link').bind("click", function(e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 130
        });
        e.preventDefault();
    });

});


function setCursorPosition(pos, elem) {
    elem.focus();
    if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
    else if (elem.createTextRange) {
        var range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd("character", pos);
        range.moveStart("character", pos);
        range.select()
    }
}

//mask for phone
function mask(event) {
    var matrix = "+7 (___) ___ ____",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");
    if (def.length >= val.length) val = def;
    this.value = matrix.replace(/./g, function (a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });

    if (event.type == "blur") {
        if (this.value.length == 2) this.value = ""
    } else setCursorPosition(this.value.length, this)
}

//few sliders
function slider_init() {
    // If there are gallery thumbs on the page

    // Thumbnail slider
    var thumbsSlider = $('.thumb-li').bxSlider({
        controls: true,
        pager: false,
        easing: 'easeInOutQuint',
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 3,
        infiniteLoop: true,
        slideMargin: 20,
        slideWidth: 200,
        responsive: true,
        captions: true,
        onSliderLoad: function(){
            $(this).css("visibility", "visible");
        }

    });

    var thumbsSliderDecor = $('.thumb-li_decor').bxSlider({
        controls: false,
        pager: true,
        easing: 'easeInOutQuint',
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 3,
        infiniteLoop: true,
        slideMargin: 5,
        slideWidth: 200,
        responsive: true,
        onSliderLoad: function(){
            $(this).css("visibility", "visible");
        }
    });


    // Cache the thumb selector for speed
    var thumb = $('.thumb-li').find('.thumb');
    var thumb2 = $('.thumb-li_decor').find('.thumb');


    // Put slider into variable to use public functions
    var gallerySlider = $('.big-slider').bxSlider({
        controls: false,
        infiniteLoop: true,
        captions: false,
        responsive: true,
        pager: false,
        mode: 'fade',
        minSlides: 1,
        maxSlides: 1,
        moveSlides: 1,
        onSliderLoad: function(){
            $(this).css("visibility", "visible");
        },

        onAfterSlide: function (currentSlideNumber) {
            thumb.removeClass('pager-active');
            thumb.eq(currentSlideNumber).addClass('pager-active');
            thumb2.removeClass('pager-active');
            thumb2.eq(currentSlideNumber).addClass('pager-active');
        },

        onNextSlide: function (currentSlideNumber) {
            thumbsSlider(currentSlideNumber, 3);
            thumbsSliderDecor(currentSlideNumber, 3);
        },

        onPrevSlide: function (currentSlideNumber) {
            thumbsSlider(currentSlideNumber, 3);
            thumbsSliderDecor(currentSlideNumber, 3);
        }
    });


    // When clicking a thumb
    thumb.click(function (e) {
        var slide = $(this).data('slide');
        $('.thumb-li').find('.thumb').removeClass('pager-active');
        $(this).addClass('pager-active');
        e.preventDefault();
        gallerySlider.goToSlide(slide);
    });

    thumb2.click(function (e) {
        var slide = $(this).data('slide');
        $('.thumb-li_decor').find('.thumb').removeClass('pager-active');
        $(this).addClass('pager-active');
        e.preventDefault();
        gallerySlider.goToSlide(slide);
    });

}

//
function repairBsModal() {
    $('.modal').each(function () {
        $(this)
            .on('shown.bs.modal', function () {
                if (!checkBodyHasClassModalOpen()) {
                    $('body').addClass('modal-open');
                }
            })
            .on('hidden.bs.modal', function () {
                if (checkAnyModalIsOpen()) {
                    $('body').addClass('modal-open');
                }
            })
    });
}

function checkBodyHasClassModalOpen() {
    return $('body').hasClass('modal-open');
}

function checkAnyModalIsOpen() {
    let isOpen = false;

    if (typeof $._isShown === 'function') {
        $('.modal').each(function () {
            if (($(this).data('bs.modal') || {})._isShown) {
                isOpen = true;
            }
        });
    } else if (typeof $.isShown === 'function') {
        $('.modal').each(function () {
            if (($(this).data('bs.modal') || {}).isShown) {
                isOpen = true;
            }
        });
    }

    return isOpen;
}